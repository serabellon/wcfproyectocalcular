﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

// NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
public class ServiceCalculator : IServiceCalculator
{
	public string GetSum(int value1, int value2)
	{
		return string.Format("The sum of numbers is: {0}", value1 + value2);
	}
}
